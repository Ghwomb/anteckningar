~.\" Manualsida för comp.
.\" Kontakta http://github.com/Ghwomb för att korrigera.
.TH man 7 "19 Juni 2014" "0.1" "comp manualsida"
.SH NAME
comp \- Kompilera ett C program med Clang.
.SH SYNOPSIS
comp [FIL]
.SH BESKRIVNING
comp används för att snabb kompilera en fil. Den tar bort en, eventuellt existerande, gammal kompilerad fil. Därefter kompilerar den FIL med Clang och gör den körbar med chmod. Sist så körs den kompilerade filen.
.SH OPTIONS
comp tar inte några växlar.
.SH INSTALLATIONSEXEMPEL
Om du installerat i ~/git/anteckningar:

Lägg till raden PATH="$HOME/git/anteckningar:$PATH" i din ~/.profile.

Det ovanstående fungarar i med bash(1).
.SH SE OCKSÅ
rm(1), clang(1), chmod(1)
.SH BUGGAR
Inga kända buggar.
.SH UPPHOVSMAN
http://github.com/Ghwomb

